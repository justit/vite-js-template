import axios from 'axios'
import useUserStore from '@/store/user'

const request = axios.create({
  baseURL: import.meta.env.VITE_API_BASEURL
})

// 请求拦截器
request.interceptors.request.use(
  config => {
    const userStore = useUserStore()
    // 容错：防止请求地址中有空格
    config.url = config.url?.trim()

    // 统一设置用户 token
    if (userStore.token) {
      config.headers.Authorization = 'Bearer ' + userStore.token
    }
    return config
  },
  error => {
    return Promise.reject(error)
  }
)

// 响应拦截器
request.interceptors.response.use(
  response => {
    // 2xx 范围内的状态码都会触发该函数。
    // 对响应数据做点什么
    // 如果是自定义状态码，错误处理写在这里
    return response
  },
  error => {
    // 超出 2xx 范围的状态码都会触发该函数。
    // 如果使用的是 HTTP 状态码, 错误处理写在这里
    // 对响应错误做点什么
    if (error.response) {
      const { data } = error.response

      ElMessage({
        type: 'error',
        message: data.error,
        duration: 5 * 1000
      })
    } else if (error.request) {
      // 请求已经成功发起，但没有收到响应
      // `error.request` 在浏览器中是 XMLHttpRequest 的实例，
      // 而在node.js中是 http.ClientRequest 的实例
      ElMessage({
        type: 'error',
        message: '请求超时，请刷新页面',
        duration: 5 * 1000
      })
    } else {
      // 发送请求时出了点问题
      ElMessage({
        type: 'error',
        message: `请求失败，${error.message}`,
        duration: 5 * 1000
      })
    }

    return Promise.reject(error)
  }
)

export default async (config) => {
  const { data } = await request(config)
  return data
}
