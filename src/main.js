import { createApp } from 'vue'
import App from './App.vue'
import plugin from '@/plugins/index'
import 'virtual:uno.css'

const app = createApp(App)

app.use(plugin)
app.mount('#app')
