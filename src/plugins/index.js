const modulesFiles = import.meta.glob('./modules/*.js', { eager: true })

export default {
  install (app) {
    Object.values(modulesFiles).forEach(module => {
      module.default(app)
    })
  }
}
