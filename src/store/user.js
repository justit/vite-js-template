export default defineStore('user', {
  state: () => {
    return {
      user: {},
      token: '',
      form: {
        cookie: false,
        username: '',
        password: ''
      }
    }
  },
  actions: {
    setUser (user = {}) {
      this.user = user
    },
    setToken (token = '') {
      this.token = token
    },
    setForm (form = {
      username: '',
      password: '',
      cookie: false
    }) {
      this.form = form
    },
    logOut () {
      // 置空用户信息和token
      this.setUser()
      this.setToken()
    }
  },
  persist: {
    enabled: true,
    strategies: [
      {
        storage: localStorage
      }
    ]
  }
})
